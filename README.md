[![build status](https://gitlab.com/Mumba/node-microservice-vault/badges/master/build.svg)](https://gitlab.com/Mumba/node-microservice-vault/commits/master)
# Mumba Microservice Vault

Provide HashiCorp Vault support for a Microservice.

## Configuration

This module requires that a `Config` object has been registered as `config`.

Property | Type | Description
--- | :---: | ---
`vault` | `object` | A dictionary of options required for a `GenericSecret` object (see `VaultOptions` interface).
`vault:host` | `string` | The hostname for the Vault server. 

## DI Container

This module adds the following container artifacts if they don't already exist.

Name | Type | Description
--- | :---: | ---
`vault` | `GenericSecret` | An instance of a [`mumba-vault`](https://gitlab.com/Mumba/node-vault) object.
`vaultOptions` | `VaultOptions` interface | A dictionary options for vault.

## Installation

```sh
$ npm install --save mumba-microservice-vault
$ typings install --save dicontainer=npm:mumba-typedef-dicontainer
```

## Examples

```typescript
const Sandal = require("sandal");
import {DiContainer} from "mumba-typedef-dicontainer";
import {Config} from "mumba-config";
import {GenericSecret} from "mumba-vault";
import {MicroserviceVaultModule} from "mumba-microservice-vault";

let container: DiContainer = new Sandal();

// Set the configuration for the logger.
// (or include the Microservice Config Module and set `defaultConfig`)
container.object('config', new Config({
	vault: {
	}
}));

// Register the module.
MicroserviceVaultModule.register(container);

container.resolve((err: Error, vault: GenericSecret) => {
	if (err) {
		throw err;
	}

	// Connect to Vault
});
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm run docker:up
$ npm install
$ npm test
$ npm docker:down
```

## People

The original author of _Mumba Microservice Vault_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-vault/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.

