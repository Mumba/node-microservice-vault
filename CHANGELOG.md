# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.4.1] 9 Mar 2018
- Updated deps.

## [0.4.0] 20 Oct 2016
- Added `preConfigureFirst`.

## [0.3.0] 19 Aug 2016
- Changed signature of `preConfigure` to take the required arguments rather than the container.

## [0.2.0] 15 Aug 2016
- Updated deps.

## [0.1.2] 10 Aug 2016
- Changed `vaultOptions` to default to the `vault` path in a `Config` object instead of an empty object.

## [0.1.1] 5 Aug 2016
- Added vault exports.

## [0.1.0] 27 Jul 2016
- Initial release
