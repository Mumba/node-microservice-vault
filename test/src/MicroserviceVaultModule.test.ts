/**
 * MicroserviceVaultModule tests
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from "assert";
import {Config} from "mumba-config";
import {DiContainer} from "mumba-typedef-dicontainer";
import {VaultOptions, GenericSecret} from "mumba-vault";
import {MicroserviceVaultModule, vaultCliOptions} from '../../src/index';

describe('MicroserviceVaultModule integration tests', () => {
	let container: DiContainer;

	beforeEach(() => {
		container = new Sandal();
		container.object('config', new Config({
			vault: {
				host: 'http://vault'
			}
		}));
	});

	it('should get the command line options', () => {
		let options = vaultCliOptions();

		assert.equal(options[0].name, 'vault', 'should be vault');
		assert.equal(options[1].name, 'vault-token', 'should be vault-token');
	});

	it('should register artifacts in the container', (done) => {
		MicroserviceVaultModule.register(container);

		assert.strictEqual(container.has('vault'), true, 'should have vault');

		container.resolve((err: Error, vaultOptions: VaultOptions, vault: GenericSecret) => {
			if (err) {
				return done(err);
			}

			assert(vault instanceof GenericSecret, 'should be a GenericSecret object');
			assert.equal(vaultOptions.host, 'http://vault', 'should set the options out of config by default');
			done();
		});
	});

	it('should pre-configure the service with vault support', () => {
		let mainModuleOptions = {
			vault: 'http://vault:9000/v1/secret/foo',
			'vault-token': 'the-token'
		};
		let config = new Config();

		MicroserviceVaultModule.preConfigureFirst(mainModuleOptions, config);

		let loaders = config.get('config:loaders');

		assert.equal(config.get('vault:host'), 'http://vault', 'should be a vault host');
		assert.equal(config.get('vault:port'), '9000', 'should be a vault port');
		assert.equal(loaders[0].name, 'vault', 'should be a vault loader');
		assert.equal(loaders[0].path, '/v1/secret/foo', 'should be a vault path');
		assert.equal(loaders[0].token, 'the-token', 'should be a vault token');
	});

	it('should not pre-configure vault support if not specified', () => {
		let mainModuleOptions = {};
		let config = new Config();

		MicroserviceVaultModule.preConfigureFirst(mainModuleOptions, config);

		let loaders = config.get('config:loaders');

		assert.equal(config.get('vault:host'), null, 'should not set vault host');
		assert.equal(config.get('vault:port'), null, 'should not set vault port');
		assert.equal(loaders, null, 'should not add a loader');
	});

	it('should do pre-configuration');
});
