/**
 * Register Vault dependencies.
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as url from 'url';
import {GenericSecret} from 'mumba-vault';
import {VaultLoader} from 'mumba-config-vault';
import {Config} from 'mumba-config';
import {DiContainer} from 'dicontainer';

export interface VaultModuleOptions {
	vault?: string;
	'vault-token'?: string
}

/**
 * Returns command line options that can be used with this module.
 *
 * @returns {{name: string, type: string}[]}
 */
export function vaultCliOptions(): any[] {
	return [
		{
			name: 'vault',
			type: String
		},
		{
			name: 'vault-token',
			type: String
		}
	];
}

/**
 * Add Vault support to a microservice.
 */
export class MicroserviceVaultModule {
	/**
	 * Register the module assets in the DI container.
	 *
	 * @param {DiContainer} container
	 */
	static register(container: DiContainer) {
		if (!container.has('vaultOptions')) {
			container.factory('vaultOptions', (config: Config) => {
				return config.get('vault') || {};
			});
		}

		if (!container.has('vault')) {
			container.service('vault', ['vaultOptions'], GenericSecret);
		}

		container.service('vaultLoader', VaultLoader);
	}

	/**
	 * Pre-configure the module.
	 *
	 * @param {MainModuleOptions} mainModuleOptions
	 * @param {Config}            config
	 */
	static preConfigureFirst(mainModuleOptions: VaultModuleOptions, config: Config) {
		if (mainModuleOptions.vault) {
			let uri = url.parse(mainModuleOptions.vault);
			let loaders: any[] = config.get('config:loaders') || [];

			config.set('vault:host', uri.protocol + '//' + uri.hostname);
			config.set('vault:port', uri.port);

			loaders.push({
				name: 'vault',
				path: uri.path,
				token: mainModuleOptions['vault-token']
			});

			config.set('config:loaders', loaders);
		}
	}

	/**
	 * Register the Vault loader with the configuration object.
	 *
	 * @param {Config}      config
	 * @param {VaultLoader} vaultLoader
	 * @param {object}      logger
	 */
	static preConfigure(config: Config, vaultLoader: VaultLoader, logger: any) {
		logger.debug('Pre-configuring Vault module.');
		config.registerLoader(vaultLoader);
	}
}

